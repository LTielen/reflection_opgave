package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.util.Helpers;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MySkeletonInvocationHandler implements InvocationHandler {
    private final MessageManager messageManager;
    private final Object myObject;

    public MySkeletonInvocationHandler(Object myObject) {
        messageManager = new MessageManager();
        this.myObject = myObject;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        switch (method.getName()) {
            case "getAddress":
                return this.getAddress();
            case "handleRequest":
                this.handleRequest(args);
                return null;
            case "run":
                return this.run(proxy);
            default:
                return null;
        }
    }

    private NetworkAddress getAddress() {
        return this.messageManager.getMyAddress();
    }

    private void handleRequest(Object[] args) {
        MethodCallMessage request = (MethodCallMessage) args[0];
        boolean methodPresent = false;
        for (Method method1 : myObject.getClass().getDeclaredMethods()) {
            if (method1.getName().equals(request.getMethodName())) {
                try {
                    methodPresent = true;
                    Map<String, String> parameters = request.getParameters();
                    //check if parameters present that don't have the correct name
                    List<String> wrongParameters = parameters.keySet().stream()
                            .filter(s -> !s.contains("arg"))
                            .collect(Collectors.toList());
                    //continue if all parameters are correct, if not throw a runtime exception
                    if (wrongParameters.isEmpty()) {
                        //there are no parameters to parse for the method. just invoke it
                        if (parameters.isEmpty()) {
                            method1.invoke(myObject);
                            sendReply(request, method1, null);
                        } else {
                            //loop over the amount of parameters and parse parameters, convert to primitive or object
                            // if needed add them to the convertedArray list to pass to invoke
                            List<Object> convertedParameters = new ArrayList<>();
                            for (int i = 0; i < parameters.keySet().size(); i++) {
                                final String j = "" + i;
                                //if it is an object, make a map with the field name as keys and the values as string
                                //Else just use the old key.
                                Map<String, String> parameter = parameters.entrySet().stream()
                                        .filter(s -> s.getKey().contains(j))
                                        .collect(Collectors.toMap(
                                                entry -> {
                                                    if (entry.getKey().contains("."))
                                                        return entry.getKey().substring(5);
                                                    else return entry.getKey();
                                                },
                                                Map.Entry::getValue
                                        ));
                                //if the parameter size = 1, it is a primitive and we can just add it to the array
                                if (parameter.size() == 1) {
                                    //if the parameterstype is not a string, convert to primitive
                                    if (method1.getParameterTypes()[i].getName().contains("String"))
                                        convertedParameters.add(parameter.get("arg" + i));
                                    else
                                        convertedParameters.add(Helpers.parseStringToPrimitive(parameter.get("arg" + i)));
                                }
                                //if it is 0, there are no parameters that have that number i in its name
                                else if (parameter.size() == 0) {
                                }
                                //the size indicates this parameter is an object and we need to convert it
                                //we get the appropriate class with the index from the method parameters
                                //create a new instance and get the right field through the parameter name
                                // and assign the appropriate value (converted if needed)
                                else {
                                    Class<?> parameterClass = method1.getParameterTypes()[i];
                                    //check if the method expects a primitive
                                    if (Helpers.isPrimitiveOrWrapper(parameterClass)) {
                                        throw new RuntimeException("the parameter is java object while the method" +
                                                " expects a primitive or a wrapper");
                                    } else {
                                        //check if the correct object is called
                                        int amountOfFields = parameterClass.getDeclaredFields().length;
                                        if (amountOfFields != parameter.keySet().size()) {
                                            throw new RuntimeException("the method expects an object as parameter at" +
                                                    " position " + i + " with " + amountOfFields + " fields");
                                        } else {
                                            Object parameterObject = parameterClass.newInstance();
                                            parameter.forEach((key, value) -> {
                                                try {
                                                    Field f = parameterObject.getClass().getDeclaredField(key);
                                                    f.setAccessible(true);
                                                    if (f.getType().getName().contains("String"))
                                                        f.set(parameterObject, value);
                                                    else f.set(parameterObject, Helpers.parseStringToPrimitive(value));

                                                } catch (NoSuchFieldException | IllegalAccessException e) {
                                                    e.printStackTrace();
                                                }
                                            });
                                            convertedParameters.add(parameterObject);
                                        }
                                    }
                                }
                            }
                            //All parameters should now be in the array
                            //convert the parameterList to an array for invoking
                            Object[] convertedParametersArray = convertedParameters.toArray();
                            method1.invoke(myObject, convertedParametersArray);
                            sendReply(request, method1, convertedParametersArray);
                        }
                        //if the parameters does not have "arg" in it, throw an exception
                    } else {
                        throw new RuntimeException("invalid parameter name");
                    }
                    //if any method cannot be invoked or an object cannot be created, throw a runtime exception
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            }
        }
        //throw a runtimeexception when the method is not found
        if (!methodPresent) throw new RuntimeException("method not found");
    }

    private Object run(Object proxy) {
        Thread thread = new Thread(() -> {
            while (true) {
                MethodCallMessage request = messageManager.wReceive();
                ((Skeleton) proxy).handleRequest(request);
            }
        });
        thread.start();
        return null;
    }

    private void sendReply(MethodCallMessage request, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        //if the method returnvalue is void, we just send an empty reply
        if (method.getReturnType().equals(Void.TYPE)) {
            reply.setParameter("result", "Ok");
        } else {
            Object returnObject;
            //if the method does not require parameters, we just invoke it
            if (method.getParameters().length == 0) {
                returnObject = method.invoke(myObject);
                //else we need to invoke with the converted parameters
            } else {
                returnObject = method.invoke(myObject, args);
            }

            //if the return value is a primitive type, we can just set it as a parameter to result
            if (Helpers.isPrimitiveOrWrapper(returnObject.getClass())) {
                reply.setParameter("result", returnObject.toString());
            }
            //the return value is an object. We need to iterate over the fields and set the message
            //parameters appropriately
            else {
                List<Field> returnObjectFields = Arrays.asList(returnObject.getClass().getDeclaredFields());
                returnObjectFields.forEach(field -> {
                    try {
                        field.setAccessible(true);
                        reply.setParameter("result." + field.getName(), field.get(returnObject).toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
        messageManager.send(reply, request.getOriginator());
    }
}
