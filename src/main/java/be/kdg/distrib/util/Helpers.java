package be.kdg.distrib.util;

public class Helpers {
    public static Object parseStringToPrimitive(String s){
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            try{
                return Double.parseDouble(s);
            }catch(Exception ex){
                if (s.length() == 1)
                    return s.charAt(0);
                else{
                    if(s.equals("true")) return true;
                    else if(s.equals("false")) return false;
                    else return s;
                }
            }
        }
    }

    public static boolean isPrimitiveOrWrapper(Class myClass) {
        return myClass.isPrimitive() || (myClass == String.class) || (myClass == Byte.class)
                || (myClass == Short.class) || (myClass == Integer.class)
                || (myClass == Long.class) || (myClass == Float.class)
                || (myClass == Double.class) || (myClass == Boolean.class)
                || (myClass == Character.class);
    }
}
