package be.kdg.distrib.stubFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class StubFactory {

    public static Object createStub(Class myInterface, String host, int port){
        if(!myInterface.isInterface())
            throw new IllegalArgumentException("Stub can only be created from an interface");
        else{
            InvocationHandler handler = new MyInvocationHandler(host, port);
            Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{myInterface}, handler);
            return result;
        }
    }
}
