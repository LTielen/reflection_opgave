package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.util.Helpers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;

import static be.kdg.distrib.util.Helpers.isPrimitiveOrWrapper;
import static be.kdg.distrib.util.Helpers.parseStringToPrimitive;

public class MyInvocationHandler implements InvocationHandler {
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public MyInvocationHandler(String host, int port) {
        this.serverAddress = new NetworkAddress(host, port);
        this.messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);
        if (args != null) {
            for (Object arg : args) {
                System.out.println("\targ = " + arg);
            }
        }
        Class c = proxy.getClass();
        for (Method m : c.getMethods()) {
            if (method.getName().equals(m.getName())) {
                System.out.println("\tmethodName = " + m.getName());
                MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), m.getName());
                if (args != null) {
                    for (int i = 0; i < args.length; i++) {
                        Class argClass = args[i].getClass();
                        if (isPrimitiveOrWrapper(argClass)) {
                            message.setParameter("arg" + i, "" + args[i]);
                        } else {
                            for (Field f : argClass.getDeclaredFields()) {
                                f.setAccessible(true);
                                message.setParameter("arg" + i + "." + f.getName(), "" + f.get(args[i]));
                            }
                        }
                    }
                }
                messageManager.send(message, serverAddress);
            }
        }
        MethodCallMessage reply = checkReply();
        Map<String, String> parameters = reply.getParameters();
        if (parameters.isEmpty()) {
            return null;
        } else if(parameters.size() == 1) {
            String result = parameters.get("result");
            if(method.getReturnType().getName().contains("String")) return result;
            else return parseStringToPrimitive(result);
        }else{
            Class returnClass = method.getReturnType();
            Object o = returnClass.newInstance();
            for(String parameter: parameters.keySet()){
                String fieldName = parameter.substring(parameter.indexOf('.')+1);
                Field f = returnClass.getDeclaredField(fieldName);
                f.setAccessible(true);
                if(!f.getType().getName().contains("String")) f.set(o, parseStringToPrimitive(parameters.get(parameter)));
                else f.set(o, parameters.get(parameter));
            }
            return o;
        }
    }


    private MethodCallMessage checkReply() {
        boolean keepListening = true;
        MethodCallMessage reply = null;
        while (keepListening) {
            reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            keepListening = false;

        }
        return reply;
    }


}
